package testbergerlist;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.google.gson.Gson;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;
import testberger.Giornata;
import testberger.Partita;



/**
 *
 * @author andrea
 */
public class TestBergerList {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException  {
        
        /*String squadre[] = {"Juventus", "Inter", "Milan", "Parma", "Roma", "Fiorentina",
            "Udinese", "Cagliari", "Lazio", "Torino"};*/
        
        
        LinkedList<Squadra> squadre = new LinkedList<>();
        squadre.add(new Squadra("Juventus"));
        squadre.add(new Squadra("Inter"));
        squadre.add(new Squadra("Milan"));
        squadre.add(new Squadra("Parma"));
        squadre.add(new Squadra("Roma"));
        squadre.add(new Squadra("Napoli"));
        squadre.add(new Squadra("Cagliari"));
        squadre.add(new Squadra("Sassuolo"));
        squadre.add(new Squadra("Atalanta"));
        squadre.add(new Squadra("Palermo"));
        squadre.add(new Squadra("Fiorentina"));
        squadre.add(new Squadra("Empoli"));
        squadre.add(new Squadra("Cesena"));
        squadre.add(new Squadra("Sampdoria"));
        squadre.add(new Squadra("Genoa"));
        squadre.add(new Squadra("Udinese"));
        squadre.add(new Squadra("Chievo"));
        squadre.add(new Squadra("Hellas Verona"));
        squadre.add(new Squadra("Torino"));
        squadre.add(new Squadra("Lazio"));
        
        
        AlgoritmoDiBerger(squadre);
            
        
    }

    public static void AlgoritmoDiBerger(LinkedList<Squadra> squadre) throws FileNotFoundException, UnsupportedEncodingException {

        try{
                String filename= "test.json";
                FileWriter fw = new FileWriter(filename,true); //the true will append the new data
                fw.write("\n\nNew Test\n");//appends the string to the file
                fw.close();
            }
            catch(IOException ioe){
                System.err.println("IOException: " + ioe.getMessage());
            }
        
        int numero_squadre = squadre.size();
        int numero_giornate = squadre.size()-1;
        LinkedList<Integer> list_giornate = new LinkedList<>();
        
        
        /* crea gli array per le due liste in casa e fuori */
        String[] casa = new String[numero_squadre / 2];
        String[] trasferta = new String[numero_squadre / 2];
        
        
        for (int i = 0; i < numero_squadre / 2; i++) {
            casa[i] = squadre.get(i).getNome();
            trasferta[i] = squadre.get(numero_squadre - 1 - i).getNome();
            //System.out.print(casa[i]+" - "+trasferta[i]+"\n");
        }
        
         
       for (int i = 0; i < numero_giornate; i++) {
            /* stampa le partite di questa giornata */
            list_giornate.add(i+1);
            System.out.printf(list_giornate.get(i)+" - ");
       }
       
       System.out.println();
       ArrayList<Integer> temp = new ArrayList<>(numero_giornate+1);
       Random rand = new Random();
       while(list_giornate.size() > 0) {
            int index = rand.nextInt(list_giornate.size());
            temp.add(list_giornate.remove(index));
        }
       for (int i = 0; i < numero_giornate; i++) {
           System.out.print(temp.get(i)+" - ");
       }
       
       
       for (int i = 0; i < numero_giornate; i++) {
            /* stampa le partite di questa giornata */
            
           System.out.printf("\n%d^ Giornata\n", temp.get(i));
            Giornata giornata = new Giornata(""+temp.get(i)+"^ Giornata");
            

            /* alterna le partite in casa e fuori */
            if (i % 2 == 0) {
                for (int j = 0; j < numero_squadre / 2; j++) {
                    System.out.printf("%d  %s - %s\n", j + 1, trasferta[j], casa[j]);
                    giornata.setPartita(new Partita(trasferta[j], casa[j]));
                }
            } else {
                for (int j = 0; j < numero_squadre / 2; j++) {
                    System.out.printf("%d  %s - %s\n", j + 1, casa[j], trasferta[j]);
                    giornata.setPartita(new Partita(casa[j],trasferta[j]));
                }
            }
            
            if(trasferta.length==1){
                
            }else{
            // Ruota in gli elementi delle liste, tenendo fisso il primo elemento
            // Salva l'elemento fisso
            String pivot = casa[0];
            //ystem.out.println("pivot: "+pivot);
            /* sposta in avanti gli elementi di "trasferta" inserendo 
             all'inizio l'elemento casa[1] e salva l'elemento uscente in "riporto" */
            String riporto = shiftRight(trasferta, casa[1]);
            ///System.out.println("riporto: "+riporto);
            /* sposta a sinistra gli elementi di "casa" inserendo all'ultimo 
             posto l'elemento "riporto" */
            shiftLeft(casa, riporto);
            // ripristina l'elemento fissoasa[0] = pivot ;
            //System.out.println("casa[0]: "+ pivot );
            casa[0] = pivot ;
            //System.out.println("casa[0]: "+ pivot );
            }
            
            Gson gson = new Gson();
            String json = gson.toJson(giornata);
            //System.out.println(json); 
            
            
            try{
                String filename= "test.json";
                FileWriter fw = new FileWriter(filename,true); //the true will append the new data
                fw.write(json+"\n");//appends the string to the file
                fw.close();
            }
            catch(IOException ioe){
                System.err.println("IOException: " + ioe.getMessage());
            }
            
            
        } 
    }
    

    public static String shiftRight(String[] awayArray, String home1) {

        String away = awayArray[awayArray.length-1];
        for (int i = awayArray.length - 1; i > 0; i--) {
            awayArray[i] = awayArray[i - 1];
        }
        awayArray[0] = home1;
        return away;

    }

    public static String[] shiftLeft(String[] homeArray, String riporto) {

        for (int i = 0; i < homeArray.length - 1; i++) {
            homeArray[i] = homeArray[i + 1];
        }
        homeArray[homeArray.length - 1] = riporto;
        return homeArray;

    }
}
