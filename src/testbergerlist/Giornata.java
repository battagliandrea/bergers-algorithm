/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testberger;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author andrea
 */
public class Giornata {
    
    private String name;
    private List<Partita> lista_partite;

    //CONSTRUCTOR
    public Giornata(String name) {
        this.name = name;
        lista_partite = new ArrayList<>();
    }
    
    
    public void setPartita(Partita partita){
        lista_partite.add(partita);
    }
}
